To run the project:

 1- Change connection string from BSynchroTest/appsettings.json "FinanceDatabase"
 2- build the solution 
 3- update database 
 4- publish on IIS


You can test the endpoints in the published version:

Get Customer Info : http://185.194.124.84:9093/api/Customer/GetCustomersAsync/1

Post Account: http://185.194.124.84:9093/api/ClientAccounts 

{
    "CustomerId" : 1,
    "InitialCredit" :500
}