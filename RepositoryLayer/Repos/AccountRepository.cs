﻿using DataAccessLayer;
using DataAccessLayer.Models;
using RepositoryLayer.DTO;
using RepositoryLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryLayer.Repos
{
    public class AccountRepository : IAccountRepository
    {
        private readonly FinanceContext _context;

        public AccountRepository(FinanceContext context)
        {
            _context = context;
        }
        public async Task<AccountDTO> Insert(AccountDTO Account)
        {
            Account account = _context.Add(new Account { CustomerId = Account.CustomerId, Balance = Account.Balance }).Entity;
            await _context.SaveChangesAsync();

            return new AccountDTO{ID = account.ID,  CustomerId = account.CustomerId , Balance = account.Balance};
        }

        public async Task<AccountDTO> Get(int Id)
        {
            Account account = await _context.Accounts.FindAsync(Id);
            return new AccountDTO
            {
                ID = account.ID,
                Balance = account.Balance,
                CustomerId = account.CustomerId
            };
        }

    }
}
