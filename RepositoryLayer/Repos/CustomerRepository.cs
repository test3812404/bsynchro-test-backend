﻿using DataAccessLayer;
using DataAccessLayer.Models;
using RepositoryLayer.DTO;
using RepositoryLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryLayer.Repos
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly FinanceContext _context;

        public CustomerRepository(FinanceContext context)
        {
            _context = context;
        }

        public async Task<CustomerDTO> GetByIdAsync(int Id)
        {
            Customer customer = await _context.Customers.FindAsync(Id);
            if (customer == null)
                return new CustomerDTO { };

            CustomerDTO customerDto =  new CustomerDTO { ID = customer.ID, Name = customer.Name, Surname = customer.Surname};
            customerDto.Accounts = _context.Accounts.Where(t => t.CustomerId == customer.ID).Select(account=>
                    new AccountDTO
                    {
                        ID = account.ID,
                        Balance = account.Balance,
                        Transactions = _context.Transactions.Where(trans=> trans.AccountId == account.ID).Select(trans =>
                            new TransactionDTO { ID = trans.ID, Amount = trans.Amount, TranType = (TranType)trans.TranType }
                        ).ToList()
                    }
                ).ToList();
            return customerDto;
        }
    }
}
