﻿using DataAccessLayer;
using DataAccessLayer.Models;
using RepositoryLayer.DTO;
using RepositoryLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryLayer.Repos
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly FinanceContext _context;

        public TransactionRepository(FinanceContext context)
        {
            _context = context;
        }

        public async Task<TransactionDTO> Insert(TransactionDTO transaction)
        {
           Transaction tran =  _context.Add(new Transaction { AccountId = transaction.AccountId, Amount = transaction.Amount, TranType = (int)transaction.TranType }).Entity;
            await _context.SaveChangesAsync();
            return new TransactionDTO{ ID =tran.ID , AccountId = tran.AccountId , Amount = tran.Amount , TranType = (TranType)tran.TranType };
        }

    }
}
