﻿using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryLayer.DTO
{
    public class TransactionDTO
    {
        public int ID { get; set; }

        public int Amount { get; set; }
        public int AccountId { get; set; }
        
        public TranType TranType { get; set; }

        public virtual AccountDTO AccountDTO { get; set; }
    }
    public enum TranType : int
    {
        Credit = 0,
        Debit = 1
    }
}
