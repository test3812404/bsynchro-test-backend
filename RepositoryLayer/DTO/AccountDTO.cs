﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryLayer.DTO
{
    public class AccountDTO
    {

        public AccountDTO()
        {
            this.Transactions = new List<TransactionDTO>();
        }

        public int ID { get; set; }
        public int Balance { get; set; }
        public int CustomerId { get; set; }
        
        public  ICollection<TransactionDTO> Transactions { get; set; }
    }
}
