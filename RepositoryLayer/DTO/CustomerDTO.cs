﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryLayer.DTO
{
    public class CustomerDTO
    {
        public CustomerDTO()
        {
            this.Accounts = new List<AccountDTO>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public ICollection<AccountDTO> Accounts { get; set; }

    }
}
