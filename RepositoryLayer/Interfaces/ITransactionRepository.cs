﻿using DataAccessLayer.Models;
using Microsoft.AspNetCore.Mvc;
using RepositoryLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryLayer.Interfaces
{
    public interface ITransactionRepository
    {
         Task<TransactionDTO> Insert(TransactionDTO transaction);
   
    }
}
