﻿using DataAccessLayer.Models;
using Microsoft.AspNetCore.Mvc;
using RepositoryLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryLayer.Interfaces
{
    public interface IAccountRepository 
    {
        Task<AccountDTO> Insert(AccountDTO account);

        Task<AccountDTO> Get(int Id);

    }
}
