﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Transaction
    {
        public int ID { get; set; }
        public int AccountId { get; set; }

        public int Amount { get; set; }

        public int TranType { get; set; }

        public virtual Account Account { get; set; }
    }

}
