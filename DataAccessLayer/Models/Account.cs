﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class Account
    {
        public int ID { get; set; }
        public int CustomerId { get; set; }
        public int Balance { get; set; }

        [NotMapped]
        public int InitialCredit { get; set; }


        public virtual Customer customer { get; set; }
    }
}
