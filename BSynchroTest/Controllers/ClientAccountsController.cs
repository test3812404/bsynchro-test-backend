﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataAccessLayer;
using DataAccessLayer.Models;
using ServiceLayer.Interfaces;
using RepositoryLayer.DTO;
using BSynchroTest.Model;

namespace BSynchroTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientAccountsController : ControllerBase
    {

        private readonly IAccountService _accountService;

        public ClientAccountsController(ICustomerService customerService , IAccountService accountService)
        {
            _accountService = accountService;

        }


        [HttpPost]
        public async Task<ActionResult<AccountDTO>> PostClientAccount(ClientAccount clientAccount)
        {
            return await _accountService.CreateAccount(clientAccount.CustomerId, clientAccount.InitialCredit);
        }



    }
}
