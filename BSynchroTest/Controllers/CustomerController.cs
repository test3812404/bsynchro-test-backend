﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RepositoryLayer.DTO;
using ServiceLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSynchroTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
            readonly ICustomerService _customerService;

            public CustomerController(ICustomerService customerService)
            {
                _customerService = customerService;
            }

            [HttpGet("GetCustomersAsync/{Id}")]
            public async Task<CustomerDTO> GetCustomersAsync(int Id)
            {
                return await _customerService.GetByIdAsync(Id);
            }

            
        
    }
}
