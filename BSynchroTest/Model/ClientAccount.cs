﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSynchroTest.Model
{
    public class ClientAccount
    {
        public int CustomerId { get; set; }
        public int InitialCredit { get; set; }

    }
}
