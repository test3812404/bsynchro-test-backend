﻿using RepositoryLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceLayer.Interfaces
{
    public interface ITransactionService
    {
        Task<TransactionDTO> CreateTransaction(int AccountId, int initalCredit);
    }
}
