﻿using RepositoryLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceLayer.Interfaces
{
    public interface IAccountService
    {
         Task<AccountDTO> CreateAccount(int CustomerId, int InitialCredit);

        Task<AccountDTO> GetAccount(int Id);

    }
}
