﻿using RepositoryLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceLayer.Interfaces
{
    public interface ICustomerService
    {
        Task<CustomerDTO> GetByIdAsync(int Id);
    }
}
