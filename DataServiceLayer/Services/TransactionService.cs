﻿using RepositoryLayer.DTO;
using RepositoryLayer.Interfaces;
using RepositoryLayer.Repos;
using ServiceLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceLayer.Services
{
    public class TransactionService:ITransactionService
    {
        private readonly ITransactionRepository _TransactionRepository ;
        public TransactionService(ITransactionRepository _transactionRepository) => _TransactionRepository = _transactionRepository;

        public async Task<TransactionDTO> CreateTransaction(int AccountId, int initalCredit)
        {
           return await _TransactionRepository.Insert(new TransactionDTO { AccountId = AccountId, Amount = initalCredit, TranType = TranType.Credit });
            
        }
    }
}
