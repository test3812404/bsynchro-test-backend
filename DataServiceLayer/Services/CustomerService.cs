﻿using RepositoryLayer.DTO;
using RepositoryLayer.Interfaces;
using ServiceLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceLayer.Services
{
    public class CustomerService:ICustomerService
    {
        private readonly ICustomerRepository _CustomerRepository;
        public CustomerService(ICustomerRepository _customerRepository) => _CustomerRepository = _customerRepository;

        public async Task<CustomerDTO> GetByIdAsync(int Id)
        {
            CustomerDTO customer = await _CustomerRepository.GetByIdAsync(Id);
            if (customer == null)
            {
                throw new Exception("Not Found");
            }
            return customer;
        }

    }
}
