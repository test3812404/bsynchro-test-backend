﻿using RepositoryLayer.DTO;
using RepositoryLayer.Interfaces;
using ServiceLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceLayer.Services
{
    public class AccountService:IAccountService
    {
        private readonly IAccountRepository _Accountrepository;
        private readonly ICustomerRepository _CustomerRepository;  
        private readonly ITransactionService _transactionService;
        public AccountService(IAccountRepository _accountrepository, ICustomerRepository _customerRepository, ITransactionService transactionService) 
        {
            _Accountrepository = _accountrepository;
            _CustomerRepository = _customerRepository;
            _transactionService = transactionService;
        }

        public async Task<AccountDTO> CreateAccount(int CustomerId , int InitialCredit)
        {
            CustomerDTO customer= await _CustomerRepository.GetByIdAsync(CustomerId);
            if(customer == null)
            { 
                throw new Exception("Not Found"); 
            }
           
            AccountDTO account = await _Accountrepository.Insert(new AccountDTO { Balance = InitialCredit, CustomerId = CustomerId });

            if (InitialCredit != 0)
            {
                await _transactionService.CreateTransaction(account.ID, InitialCredit);
            }

            return account;

        }

        public async Task<AccountDTO> GetAccount(int Id)
        {
            return await _Accountrepository.Get(Id);
        }
    }
}
